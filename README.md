# FanActuator

ESP32 code to perform speed control of a fan

This code is suited for both a 3 pin or 4 pin fan, but additional electronics
needed for a 3 pin fan. the 4 pin fans have a built in pwm channel

Connect the pwm fan pin to pin 25 on the ESP
