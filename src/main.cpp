#include <Arduino.h>

#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include <ArduinoJson.h>

SSD1306Wire  display(0x3c, 5, 4);

IOTGateway iotGateway(&capgeminiHackathon);

int msgReceived = 0;
String payload;
String rcvdPayload;
int latest = 0;
bool change = true;

void callBackHandler (char *topicName, int payloadLen, char *payLoad) {
  char rcvdPayloadChar[512];

  strncpy(rcvdPayloadChar, payLoad, payloadLen);
  rcvdPayloadChar[payloadLen] = 0;
  rcvdPayload = rcvdPayloadChar;

  StaticJsonDocument<512> message;
  DeserializationError err = deserializeJson(message, rcvdPayload);

  if (err) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(err.c_str());
  } else {
    latest = message["value"];
    msgReceived = 1;
  }
}
 
void receiveMessage() {
  if(msgReceived == 1) {
    msgReceived = 0;
    change = true;
    Serial.print("Received: ");
    Serial.println(latest);
  }
}

// the number of the LED pin
const int pwmPin = 25;  // 16 corresponds to GPIO16

// setting PWM properties
const int freq = 25000;
const int pwmChannel = 0;
const int resolution = 8;

void setup() {
  Serial.begin(115200);
  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();
  display.clear();
  Serial.println("Setup complete");

  display.drawString(0,0,"Connecting");
  display.display();
  iotGateway.initialise();
  display.clear();
  display.drawString(0,0,"Subscribing");
  display.display();

  delay(2500);
  iotGateway.subscribe("command/fan", callBackHandler);
  display.clear();
  display.display();

  delay(2500);

  ledcSetup(pwmChannel, freq, resolution);
  ledcAttachPin(pwmPin, pwmChannel);
}

char tempBuffer [10];

void displayPWM(int value) {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "fan speed");
    display.setFont(ArialMT_Plain_16);
    sprintf (tempBuffer, "%i%%", value);
    display.drawString(65, 0, tempBuffer);
    display.display();
}

void setFanSpeed(int percentage)
{
  //128 instead of zero since there's much going on below that Value
  const int dutyCycle  = map(percentage, 0, 100, 0, 255);
  ledcWrite(pwmChannel, dutyCycle);
}

void loop() {

  receiveMessage();

  if (change)
  {
    setFanSpeed(latest);
    displayPWM(latest);
    change = false;
  }
}
